Scramled Exif (<i>eg</i>sif olarak telaffuz edilir) görsellerinizi paylaşmadan önce görsellerde yer alan metaverileri (üstveri) silmenizi sağlar.

Eğer beğendiyseniz.

Eğer istismarcı internet şirketlerinin fotoğraflarınızın nerede çekildiğini bilmesini istemiyorsanız metaverilerden kurtulmayı unutmayın.

Metaverilerden kurtulmak için paylaş menüsünde <i>Scramled Exif uygulamasını</i> seçin ve tekrardan aynı menünün gelmesini bekleyin. Artık görselinizi istediğiniz uygulama üzerinden paylaşabilirsiniz.

Bu kadar basit!

<b>Gerekli Android izinleri</b>

★ READ_EXTERNAL_STORAGE izni, diğer paylaşılan görselleri okumak için istenir.

<b>Kaynak kodu</b>

★ Bu uygulama açık kaynak kodlu: Koda göz atabilir ve beğendiyseniz buradan katkıda bulunabilirsiniz:

https://gitlab.com/juanitobananas/scrambled-exif

<b>Çeviriler</b>

Eğer bu uygulamayı kendi dilinize tercüme etmek isterseniz Transifex'i kullanarak bu iki projeye katkıda bulunabilirsiniz:
https://www.transifex.com/juanitobananas/scrambled-exif/ ve https://www.transifex.com/juanitobananas/libcommon/.

<b>Çeşitli bilgiler</b>

★ Exif, Android'ın çektiği fotoğrafları kaydettiği format olan jpeg tarafından kullanılır. Exif hakkında daha fazla öğrenmek için Wikipedia'ya göz atın.

★ Scrambled Exif uygulaması aynı zamanda dosyaları yeniden adlandırabilir (bu özelliği kapatabilirsiniz).

★ Eşeğinizi sağlam kazığa bağladınız fakat kazığa tamamen güvenmeyin; Scrambled Exif işini gayet iyi yapar. Buna rağmen paylaşmadan önce bir göz atmakta fayda var.

★ Aslında bu uygulama, isminde geçtiği gibi, Exif verisini <i>karıştırmaz</i>; siler. Bu yüzden isim yüksek ihtimalle aptalca. Ne var ki benim hoşuma gidiyor. İkon da çırpılmış yumurta değil. Bu yüzden ikon da yüksek ihtimalle aptalca. Ne var ki benim hoşuma gidiyor. Ayrıca yumurtaların büyük bir fanıyımdır. Toparlamak gerekirse bu uygulama aptalca bir isme ve ikona sahip olmasının yanında, benim yumurtaları şereflendirdiğim bir olgu. Özellikle <i>huevos fritos</i>. Çünkü ona bayılırım.

★ İyi paylaşmalar!